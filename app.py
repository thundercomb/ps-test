from aitextgen import aitextgen
from spacy.lang.en import English
from spacy.tokenizer import Tokenizer
from starlette.applications import Starlette
from starlette.responses import UJSONResponse
from starlette.templating import Jinja2Templates
import uvicorn
import sys

#ai = aitextgen(model="trained_model/pytorch_model.bin", config="trained_model/config.json", to_gpu=True)
ai = aitextgen(to_gpu=True)

templates = Jinja2Templates(directory='templates')
app = Starlette(debug=False)

nlp = English()
sentencizer = nlp.create_pipe("sentencizer")
nlp.add_pipe(sentencizer)

sl_index = 0

@app.route('/', methods = ['GET', 'POST'])
async def home(request):
    # Set global vars
    global sess

    # Clear relevant vars
    next_text = ""
    text = []
    user_input, warning = "", ""
    doc, doc_sents, new_doc, new_doc_sents = None, None, None, None

    # Take new user input and generate new story
    if request.method == 'POST':
        # Get user input from the form
        form_input = await request.form()

        # Otherwise it is safe to assume it is text input
        user_input = form_input.get('user_input') + " "

        # Ensure closing quotes
        if not user_input.find('“'):
            if int(user_input.count('"') % 2) != 0:
                user_input = user_input + '"'
        else:
            if user_input.rfind('“') > user_input.rfind('”'):
                user_input = user_input + '”'

        # Convert plain quotes to curly quotes if needed
        # https://stackoverflow.com/questions/58567783/how-to-convert-straight-quotes-to-curly-quotes-in-python
        for i in range(int(user_input.count('"')/2.0+1)):
            user_input = user_input.replace( '"', '“', 1)
            user_input = user_input.replace( '"', '”', 1)

        # Construct prefix
        prefix = user_input

        # Generate text
        print("generating text ...")
        text = ai.generate_one(max_length=250,
                               prompt=prefix,
                               temperature=1.0,
                               top_k=1000,
                               top_p=0.9,
                               num_beams=2,
                               repetition_penalty=1.3
        )

        # Remove redundant chars and strings
        print(text)
        text = text.replace(prefix,'') # remove prefix
        for s in ['*','-','~','#']:
            text = text.replace(s,'') if text.find(f'{s}{s}') > -1 else text
        for s in ['<|startoftext|>','<|endoftext|>','[PROMPT]','[RESPONSE]']:
            text = text.replace(s,'')
        print(f">>> text {i} replaced: ", text)

        # Get rid of the final sentence if it is not a full sentence and set new_story
        doc = nlp(text)
        doc_sents = list(doc.sents)
        if len(doc_sents) > 0:
            test_doc = nlp(str(doc_sents[-1]) + " X")
            if len(list(test_doc.sents)) == 1:
                next_text = text[:-len(str(doc_sents[-1]))]
            else:
                next_text = text

            # Ensure closing quotes
            if int(next_text.count('"') % 2) != 0:
                next_text = next_text + '"'

        new_text = f'{prefix} {next_text}'

    return templates.TemplateResponse('index.html', {
                                      'request': request,
                                      'new_story': new_text,
                                      }
    )

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=5000)
